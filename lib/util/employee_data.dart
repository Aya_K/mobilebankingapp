import 'package:dio/dio.dart';
import 'package:mobilebanking/models/employee.dart';

class EmployeeData {
  Dio dio = new Dio();
  Response response;
  Future<List<Employee>> getData() async {
    response =
        await dio.get("http://dummy.restapiexample.com/api/v1/employees");
    if (response.statusCode == 200) {
      var data = response.data['data'] as List;
      List<Employee> employees =
          List<Employee>.from(data.map((e) => Employee.fromJson(e)));
      return employees;
    } else {
      print("Error When getting data");
      return null;
    }
  }

  void createEmployee(String name, String salary, String age) async {
    response = await dio.post("http://dummy.restapiexample.com/api/v1/create",
        data: {
          'employee_name': name,
          'employee_salary': salary,
          'employee_age': age
        });
    print("--------------------------------------------------------------");
    print(response);
  }

  void updateEmployee(String id, String name, String salary, String age) async {
    response = await dio.put(
        "http://dummy.restapiexample.com/api/v1/update/$id",
        data: {'id': id,'employee_name': name,
          'employee_salary': salary,
          'employee_age': age});
    print(response);
  }

  void deleteEmployee(String id) async {
//    dio.interceptors.add(LogInterceptor(requestBody: true , responseBody: true));
    response =
        await dio.delete("http://dummy.restapiexample.com/api/v1/delete/$id");
    print("--------------------------------------------------------------");
    print(response);
    print(response.data['data']);
  }
}
