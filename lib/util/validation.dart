
class Validator{
  static bool validateUserName(String name) => name.length >= 2;
  static bool validateEmail(String email) =>
      !RegExp(r'[\w\d]{4,}@[\w\d]{2,}\.[\w\d]+').hasMatch(email);
  static bool validatePhone(String phone) => phone.length == 10 || phone.length == 9;
  static bool validatePassword(String password) => password.length >= 8;
}