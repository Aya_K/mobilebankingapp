import 'package:flutter/material.dart';
import 'package:mobilebanking/models/historyItem.dart';

class HistoryList{
  List<HistoryItem> get historyItems => [
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
    HistoryItem(Icons.email, Color(0xff24346C), "Apple Device",
        "13:40, 10 Aug 2020", 812.50),
  ];
}