import 'package:flutter/material.dart';

abstract class Routers {
  static void push(BuildContext context, Widget page) =>
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => page));
}
