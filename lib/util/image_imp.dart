import 'dart:io';
import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';

class ImgImp {
  final picker = ImagePicker();
  Response response;
  Dio dio = Dio();
  void putImage() async {
    File imgFile = await getImage();
    var img = imgFile.path.split('/').last;
    FormData data = FormData.fromMap({
      "image" : await MultipartFile.fromFile(imgFile.path, filename: img)
    });
    response =
        await dio.post("https://httpbin.org/post", data: data);
    if (response.statusCode == 200) {
      print(response.toString());
    } else
      print('ERROR');
  }

  Future<File> getImage() async {
    var pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      return File(pickedFile.path);
    } else {
      return null;
    }
  }
}