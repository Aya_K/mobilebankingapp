import 'package:flutter/material.dart';
import 'package:mobilebanking/models/employee.dart';
import 'package:mobilebanking/screens/update_employee_screen.dart';
import 'package:mobilebanking/util/employee_data.dart';

class EmployeeScreen extends StatelessWidget {
  const EmployeeScreen(this.employee);
  final Employee employee;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    bool isDeleted ,isUpdated = false;

    return Scaffold(
      appBar: AppBar(title: Text("Employee"), centerTitle: true),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: screenHeight * .15,
            ),
            SizedBox(
              height: screenHeight * .3,
              width: screenWidth * .7,
              child: Card(
                elevation: 10.0,
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(" ID : " + employee.id.toString()),
                      Text(" Name : " + employee.employee_name.toString()),
                      Text(" Salary : " + employee.employee_salary.toString()),
                      Text(" Age : " + employee.employee_age.toString()),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: screenHeight * .06,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: screenWidth * .4,
                  height: 55,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(
                        "Update",
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                      color: Color(0xff24346C),
                      onPressed: () =>  Navigator.of(context).push(MaterialPageRoute(builder: (_) => UpdateEmployeeScreen(employee))),),
                ),
                SizedBox(
                  width: screenWidth*.4,
                  height: 55,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text("Delete" , style: TextStyle(color: Colors.white, fontSize: 20),),
                      color: Color(0xff24346C),
                      onPressed: ()=> {
                        EmployeeData().deleteEmployee(employee.id.toString()),
                        isDeleted = true,
                        Navigator.pop(context , isDeleted),
                      }
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
