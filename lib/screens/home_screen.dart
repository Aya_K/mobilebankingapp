import 'package:flutter/material.dart';
import 'package:mobilebanking/widgets/Label.dart';
import 'package:mobilebanking/widgets/history_sheet.dart';
import 'package:mobilebanking/widgets/operation_button.dart';
import 'package:mobilebanking/widgets/paint/home_paint.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen();
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HomePaint(screenHeight, screenWidth),
              Padding(
                padding: EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Label("Popular operations"),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        OperationButton(
                          Icons.home,
                          Color(0xff24346C),
                          label: "All",
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        OperationButton(
                          Icons.favorite,
                          Color(0xffF56962),
                          label: "Health",
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        OperationButton(
                          Icons.airplanemode_active,
                          Color(0xff24346C),
                          label: "Travel",
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        OperationButton(
                          Icons.local_mall,
                          Color(0xffF56962),
                          label: "Food",
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
          HistorySheet(
            screenHeight,
            screenWidth,
            "History",
            IconButton(
              icon: Icon(Icons.search),
              iconSize: 27,
              color: Color(0xffB3B3B3),
              onPressed: () {},
            ),
            scrollable: true,
          ),
//          DraggableScrollableSheet(
//              initialChildSize: 0.25,
//              expand: true,
//              maxChildSize: 0.87,
//              builder: (context, scrollController) {
//                return SingleChildScrollView(
//                  controller: scrollController,
//                  child: SizedBox(
//                    height: screenHeight*.8,
//                    child: Card(
//                      shape: RoundedRectangleBorder(
//                          borderRadius: BorderRadius.circular(24)),
//                      margin: const EdgeInsets.all(0),
//                      child: ListView.builder(
//                            shrinkWrap: true,
//                            controller: scrollController,
//                            itemCount: historyContent.length,
//                            itemBuilder: (BuildContext context, int index) =>
//                               historyContent[index],
//                            scrollDirection: Axis.vertical,
//                          ),
//                    ),
//                  ),
//                );
//              }),
        ],
    );
  }
}
