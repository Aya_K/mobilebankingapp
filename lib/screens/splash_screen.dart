import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:mobilebanking/screens/signin_screen.dart';
import 'package:mobilebanking/screens/signup_screen.dart';
import 'package:mobilebanking/util/router.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Routers.push(context,SignInScreen());
  }

  Widget _buildImage(String assetName) {
    return Align(
      child: Image.asset(
        'assets/$assetName.png',
        width: 332.0,
      ),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 17.0, color: Colors.white);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(
          fontSize: 28.0, fontWeight: FontWeight.w700, color: Colors.white),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Color(0xff24346C),
      imagePadding: EdgeInsets.zero,
    );

    return Scaffold(
      body: IntroductionScreen(
        key: introKey,
        onDone: () => _onIntroEnd(context),
        onSkip: () => _onIntroEnd(context), // You can override onSkip callback
        showSkipButton: true,
        skipFlex: 0,
        nextFlex: 0,
        showNextButton: false,
        skip: Text('Skip' , style: TextStyle(color: Colors.white),),
        done: Text('Continue', style: TextStyle(fontWeight: FontWeight.w600 , color: Colors.white)),
        pages: [
          PageViewModel(
            title: "Mobile banking",
            body:
                "Mobile banking is a service provided by a bank or other finacial institution that allows its customers to conduct.",
            image: _buildImage('logo1'),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: "Easy to booking",
            body:
                "If you've been using our credit cards just to fill your trank and buy groceries, you might be passing up a host of travel.",
            image: _buildImage('logo2'),
            decoration: pageDecoration,
          ),
          PageViewModel(
            title: "Easy to shop",
            body:
                "Making it as easy as possible for your customers to pay is essential for increasing conversions and sales.",
            image: _buildImage('logo3'),
            decoration: pageDecoration,
          ),
        ],

        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeSize: Size(40.0, 10.0),
          activeColor: Colors.white,
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
        ),
      ),
    );
  }
}
