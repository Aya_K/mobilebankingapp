import 'package:flutter/material.dart';
import 'package:mobilebanking/screens/signin_screen.dart';
import 'package:mobilebanking/util/router.dart';
import 'package:mobilebanking/widgets/paint/signup_paint.dart';
import 'package:mobilebanking/widgets/signup_form.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SignUpPaint(onPressed: ()=> Routers.push(context,SignInScreen()),),
            Padding(
              padding: EdgeInsets.only(top:40.0, left: 40.0 , right: 40.0 , bottom: 5.0),
              child: SignUpForm(),
            ),
          ],
        ),
      ),
    );
  }
}
