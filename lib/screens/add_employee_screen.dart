import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobilebanking/util/employee_data.dart';
import 'package:mobilebanking/widgets/Label.dart';
import 'package:mobilebanking/widgets/button.dart';
import 'package:mobilebanking/widgets/field.dart';

class AddEmployeeScreen extends StatefulWidget {
  const AddEmployeeScreen();

  @override
  _AddEmployeeScreenState createState() => _AddEmployeeScreenState();
}

class _AddEmployeeScreenState extends State<AddEmployeeScreen> {
  var nameController = TextEditingController();
  var salaryController = TextEditingController();
  var ageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(title: Text("Add Employee"), centerTitle: true,),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 40,
                ),
                Label("Name"),
                SizedBox(
                  height: 10,
                ),
                Field(
                  'test',
                  controller: nameController,
                  validator: (nameController) {
                    if (nameController.isEmpty) {
                      return "Name is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Label("Salary"),
                SizedBox(
                  height: 10,
                ),
                Field('123', controller: salaryController,keyBoardType: TextInputType.number,
                  validator: (passwordController) {
                    if (passwordController.isEmpty) {
                      return "Salary is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 5,
                ),
                Label("Age"),
                SizedBox(
                  height: 10,
                ),
                Field('22', controller: ageController,keyBoardType: TextInputType.number,
                  validator: (ageController) {
                    if (ageController.isEmpty) {
                      return "Age is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: screenHeight * 0.1,
                ),
                Button(
                    "Add",
                    onPressed: () {
                      EmployeeData().createEmployee(nameController.text, salaryController.text, ageController.text);
                    })
              ]),
        ),
      ),);
  }
}