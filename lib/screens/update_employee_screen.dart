import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobilebanking/models/employee.dart';
import 'package:mobilebanking/util/employee_data.dart';
import 'package:mobilebanking/widgets/Label.dart';
import 'package:mobilebanking/widgets/button.dart';
import 'package:mobilebanking/widgets/field.dart';

class UpdateEmployeeScreen extends StatefulWidget {
  const UpdateEmployeeScreen(this.employee);
  final Employee employee;
  @override
  _UpdateEmployeeScreenState createState() => _UpdateEmployeeScreenState();
}

class _UpdateEmployeeScreenState extends State<UpdateEmployeeScreen> {
  var nameController = TextEditingController();
  var salaryController = TextEditingController();
  var ageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        title: Text("Update Employee"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 40,
                ),
                Label("Name"),
                SizedBox(
                  height: 10,
                ),
                Field(
                  widget.employee.employee_name,
                  controller: nameController,
                  validator: (nameController) {
                    if (nameController.isEmpty) {
                      return "Name is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Label("Salary"),
                SizedBox(
                  height: 10,
                ),
                Field(
                  widget.employee.employee_salary.toString(),
                  controller: salaryController,
                  keyBoardType: TextInputType.number,
                  validator: (passwordController) {
                    if (passwordController.isEmpty) {
                      return "Salary is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 5,
                ),
                Label("Age"),
                SizedBox(
                  height: 10,
                ),
                Field(
                  widget.employee.employee_age.toString(),
                  controller: ageController,
                  keyBoardType: TextInputType.number,
                  validator: (ageController) {
                    if (ageController.isEmpty) {
                      return "Age is required";
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: screenHeight * 0.1,
                ),
                Button("Update", onPressed: () {
                  EmployeeData().updateEmployee(widget.employee.id.toString(),nameController.text,
                      salaryController.text, ageController.text);
                })
              ]),
        ),
      ),
    );
  }
}