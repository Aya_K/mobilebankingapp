import 'package:flutter/material.dart';
import 'package:mobilebanking/screens/signup_screen.dart';
import 'package:mobilebanking/util/router.dart';
import 'package:mobilebanking/widgets/paint/sigin_paint.dart';
import 'package:mobilebanking/widgets/signin_form.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen();



  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    void go(){Routers.push(context,SignUpScreen());}

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SignInPaint(screenWidth, screenHeight , onPressed: ()=>go()),
            Padding(
              padding: EdgeInsets.only(top:40.0, left: 40.0 , right: 40.0),
              child: SignInForm(),
            ),
          ],
        ),
      ),
    );
  }
}
