import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobilebanking/screens/signin_screen.dart';
import 'package:mobilebanking/util/router.dart';
import 'package:mobilebanking/util/util.dart';
import 'package:mobilebanking/widgets/profile_items.dart';
import 'package:mobilebanking/widgets/text_title.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen();
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(27.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TxtTitle("Profile"),
              IconButton(
                  icon: Icon(
                    Icons.more_vert,
                    size: 35,
                    color: Colors.black,
                  ),
                  onPressed: null)
            ],
          ),
        ),
        Center(
          child: Column(
            children: [
              Stack(
                children: [
                  Image.asset(
                    "assets/profile.png",
                    width: 120,
                    height: 120,
                  ),
                  Positioned(
                    width: screenWidth * .12,
                    height: screenHeight * .29,
                    child: SizedBox(
                      child: FloatingActionButton(
                        backgroundColor: Colors.white,
                        mini: true,
                        child: FaIcon(
                          FontAwesomeIcons.edit,
                          color: Color(0xff24346C),
                        ),
                        onPressed: () {},
                      ),
                    ),
//                        IconButton(
//                            icon: FaIcon(FontAwesomeIcons.edit , color: Color(0xff24346C),),
//                            onPressed: null)
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Kristin Waston",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 22),
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                "330 265 4334",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
        SizedBox(
          height: screenHeight * 0.05,
        ),
        Expanded(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ProfileItems(Icons.credit_card, Colors.pink[100], Colors.pink,
                "Order Extra Card" , onPressed: (){},),
            ProfileItems(Icons.person_outline, Colors.blue[100], Colors.blue,
                "Personal Details" , onPressed: (){},),
            ProfileItems(Icons.lock_outline, Colors.pink[100], Colors.pink,
                "Privacy & Security",  onPressed: (){},),
            ProfileItems(Icons.error_outline, Colors.orange[100], Colors.orange,
                "Legal" , onPressed: (){},),
            ProfileItems(
                Icons.exit_to_app, Colors.pink[100], Colors.pink, "Log out" , onPressed: (){
                  Util.userStorage.erase();
                  Routers.push(context, SignInScreen());
            },),
          ],
        )),
      ],
    );
  }
}
