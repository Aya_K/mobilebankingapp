import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mobilebanking/models/employee.dart';
import 'package:mobilebanking/screens/add_employee_screen.dart';
import 'package:mobilebanking/screens/employee_screen.dart';
import 'package:mobilebanking/util/employee_data.dart';
import 'package:mobilebanking/util/image_imp.dart';
import 'package:mobilebanking/widgets/Label.dart';

class AnalyticScreen extends StatefulWidget {
  const AnalyticScreen();

  @override
  _AnalyticScreenState createState() => _AnalyticScreenState();
}

class _AnalyticScreenState extends State<AnalyticScreen> {
  List<Employee> employeeList = [];
 File imgFile;
  @override
  void initState() {
    employees();
    super.initState();
  }

  Future<void> employees() async {
    employeeList = await EmployeeData().getData();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: employeeList.isEmpty
            ? Center(child: CircularProgressIndicator())
            : Column(
                children: [
                  Expanded(
                    child: ListView.builder(
                      itemCount: employeeList.length,
                      itemBuilder: (_, index) => Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: SizedBox(
                          height: screenHeight * .15,
                          width: screenWidth * .1,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(
                                      builder: (_) =>
                                          EmployeeScreen(employeeList[index])))
                                  .then((value) {
                                if (value) employees();
                              });
                            },
                            child: Card(
                              elevation: 10.0,
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Label(employeeList[index].employee_name),
                                    Text(employeeList[index].id.toString()),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight * .03,
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      FloatingActionButton(
                        backgroundColor: Color(0xff24346C),
                        child: Icon(Icons.person_add),
                        onPressed: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(
                                  builder: (_) => AddEmployeeScreen()))
                              .then((value) {
                            if (value) {
                              employees();
                            }
                          });
                        },
                      ),
                      FlatButton(onPressed: () async{
                      ImgImp().putImage();
//                        imgFile = await ImgImp().getImage();
                      setState(() {
                      });
                      },child: Icon(Icons.add_a_photo),),
//                      FloatingActionButton(
//                        backgroundColor: Color(0xff24346C),
//                        child: Icon(Icons.add_a_photo),
//                        onPressed: () async{
//                          ImgImp().putImage();
////                        imgFile = await ImgImp().getImage();
//                        setState(() {
//                        });
//                        },
//                      )
                    ],
                  ),
                  imgFile != null ? Image.file(imgFile , height: screenHeight*.1,) : Text("there is no image"),
                ],
              ),
      ),
    );
  }
}