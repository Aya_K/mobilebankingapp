import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobilebanking/widgets/history_sheet.dart';
import 'package:mobilebanking/widgets/paint/circle_painter.dart';
import 'package:mobilebanking/widgets/paint/oval_paint.dart';

class HistoryScreen extends StatelessWidget {
  const HistoryScreen();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            children: [
              CustomPaint(
                painter: OvalPainter(
                    Color(0xff24346C),
                    Size(screenWidth * 1.4, screenHeight * 0.5),
                    screenWidth * -0.2,
                    screenHeight * -0.15),
              ),
              CustomPaint(
                painter: CirclePainter(Color(0xffF56962), screenWidth * 0.5,
                    screenWidth * 0.84, screenHeight * -0.1),
              ),
              CustomPaint(
                painter: CirclePainter(Color(0xffF6CAC2), screenWidth * 0.3,
                    screenWidth * 0.8, screenHeight * 0.26),
              ),
              Positioned(
                top: screenHeight * 0.062,
                left: screenWidth * .05,
                child: Text(
                  "History",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 100.0),
                child: HistorySheet(
                  screenHeight,
                  screenWidth,
                  "Today",
                  Text(
                    "Statistic",
                    style: TextStyle(fontSize: 20, color: Color(0xffB3B3B3)),
                  ),
                ),
              ),
            ],
          ),
        ],
    );
  }
}
