import 'package:flutter/material.dart';

class Label extends StatelessWidget {
  const Label(this.txt);
  final String txt;

  @override
  Widget build(BuildContext context) {
    return Text(
      txt,
      style: TextStyle(color: Colors.black ,fontSize: 16.5,fontWeight: FontWeight.w500 ),
    );
  }
}
