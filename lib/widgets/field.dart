import 'package:flutter/material.dart';

class Field extends StatelessWidget {
  const Field(this.hint,
      {this.obscure = false,
      this.controller,
      this.keyBoardType = TextInputType.text,
      @required this.validator});
  final String hint;
  final bool obscure;
  final TextEditingController controller;
  final String Function(String) validator;
  final TextInputType keyBoardType;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: obscure,
      keyboardType: keyBoardType,
      decoration: InputDecoration(
        hintText: hint,
        border: OutlineInputBorder(),
      ),
      validator: validator,
//          (value) {
//        if (value.isEmpty) {
//          return 'Please enter some text';
//        }
//        return null;
//      },
    );
  }
}
