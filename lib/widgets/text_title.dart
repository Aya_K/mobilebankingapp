import 'package:flutter/material.dart';

class TxtTitle extends StatelessWidget {
  const TxtTitle(this.txt, {this.txtColor = Colors.black});
  final String txt;
  final Color txtColor;

  @override
  Widget build(BuildContext context) {
    return Text(
      txt,
      style: TextStyle(fontSize: 26, fontWeight: FontWeight.w800, color: txtColor),
    );
  }
}
