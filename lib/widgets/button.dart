import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button(this.label , {@required this.onPressed});
  final String label;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) => SizedBox(
    width: 500,
    height: 55,
    child: RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(label , style: TextStyle(color: Colors.white, fontSize: 20),),
      color: Color(0xff24346C),
      onPressed: onPressed,
    ),
  );
}