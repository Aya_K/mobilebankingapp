import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobilebanking/models/user.dart';
import 'package:mobilebanking/screen.dart';
import 'package:mobilebanking/util/router.dart';
import 'package:mobilebanking/util/util.dart';
import 'package:mobilebanking/util/validation.dart';
import 'package:mobilebanking/widgets/Label.dart';
import 'package:mobilebanking/widgets/button.dart';
import 'package:mobilebanking/widgets/field.dart';

class SignUpForm extends StatefulWidget {
  @override
  SignUpFormState createState() {
    return SignUpFormState();
  }
}

class SignUpFormState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var userNameController = TextEditingController();
    var emailController = TextEditingController();
    var phoneController = TextEditingController();
    var passwordController = TextEditingController();
    var confirmPasswordController = TextEditingController();

    return Form(
        key: _formKey,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
            Widget>[
          Label("User name"),
          SizedBox(
            height: 5,
          ),
          Field(
            'Kristin Watson',
            controller: userNameController,
            validator: (userNameController) {
              if (userNameController.isEmpty) {
                return "UserName is required";
              }
              if (!Validator.validateUserName(userNameController.toString())) {
                return "Invalid UserName";
              }
              return null;
            },
          ),
          SizedBox(
            height: 5,
          ),
          Label("Email"),
          SizedBox(
            height: 5,
          ),
          Field(
            'username@gmail.com',
            controller: emailController,
            validator: (emailController) {
              if (emailController.isEmpty) {
                return "Email is required";
              }
              if (!Validator.validateEmail(emailController)) {
                return "Invalid Email";
              }
              return null;
            },
          ),
          SizedBox(
            height: 5,
          ),
          Label("Phone"),
          SizedBox(
            height: 5,
          ),
          Field(
            '(408) 613 - 1120',
            controller: phoneController,
            keyBoardType: TextInputType.number,
            validator: (phoneController) {
              if (phoneController.isEmpty) {
                return "Phone is required";
              }
              if (!Validator.validatePhone(phoneController)) {
                return "Invalid Phone";
              }
              return null;
            },
          ),
          SizedBox(
            height: 5,
          ),
          Label("Password"),
          SizedBox(
            height: 5,
          ),
          Field(
            '*****',
            obscure: true,
            controller: passwordController,
            validator: (passwordController) {
              if (passwordController.isEmpty) {
                return "Password is required";
              }
              if (!Validator.validatePassword(passwordController)) {
                return "Password Length more than 7 characters";
              }
              return null;
            },
          ),
          SizedBox(
            height: 5,
          ),
          Label("Confirm Password"),
          SizedBox(
            height: 5,
          ),
          Field(
            '********',
            obscure: true,
            controller: confirmPasswordController,
            validator: (confirmPasswordController) {
              if (confirmPasswordController.isEmpty) {
                return "Password is required";
              }
//                  if (confirmPasswordController != passwordController) {
//                    return "Passwords not the same";
//                  }
              return null;
            },
          ),
          SizedBox(
            height: 5,
          ),
          Button(
            "Sign Up",
            onPressed: () {
              if (_formKey.currentState.validate()) {
                    User user = User(
                        username: userNameController.text,
                        email: emailController.text,
                        phone: phoneController.text,
                        password: passwordController.text);

                    Util.userStorage.write("user",user.toMap());
                    Routers.push(context, Screen());

//                Scaffold.of(context)
//                    .showSnackBar(SnackBar(content: Text("Processing Data")));
              }
            },
          ),
        ]));
  }
}
