import 'package:flutter/material.dart';
import 'package:mobilebanking/screen.dart';
import 'package:mobilebanking/util/router.dart';
import 'package:mobilebanking/util/validation.dart';
import 'package:mobilebanking/widgets/Label.dart';
import 'package:mobilebanking/widgets/button.dart';
import 'package:mobilebanking/widgets/field.dart';

class SignInForm extends StatefulWidget {
  @override
  SignInFormState createState() {
    return SignInFormState();
  }
}

class SignInFormState extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();
  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    double screenHeight = MediaQuery.of(context).size.height;

    return Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Label("Email"),
            SizedBox(
              height: 10,
            ),
            Field(
              'username@gmail.com',
              controller: emailController,
              validator: (emailController) {
                if (emailController.isEmpty) {
                  return "Email is required";
                }
                if (!Validator.validateEmail(emailController)) {
                  return "Invalid Email";
                }
                return null;
              },
            ),
            SizedBox(
              height: 20,
            ),
            Label("Password"),
            SizedBox(
              height: 10,
            ),
            Field('******', obscure: true, controller: passwordController,
              validator: (passwordController) {
                if (passwordController.isEmpty) {
                  return "Password is required";
                }
                if (!Validator.validatePassword(passwordController)) {
                  return "Incorrect Password";
                }
                return null;
              },
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Checkbox(
                  value: false,
                  onChanged: null,
                ),
                Text(
                  "Forget your password?",
                  style: TextStyle(fontSize: 15),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight * 0.1,
            ),
            Button(
              "Sign In",
              onPressed: () {
                if (_formKey.currentState.validate()) {
//                  Scaffold.of(context)
//                      .showSnackBar(SnackBar(content: Text("Processing Data")));
                  Routers.push(context, Screen());
                }
              },
            ),
          ],
        ));
  }
}