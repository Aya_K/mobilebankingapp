import 'package:flutter/material.dart';

class CreditCard extends StatelessWidget {
  const CreditCard( this.cardHeight , this.cardWidth, this.cardColor, {this.creditItem});
  final Color cardColor;
  final double cardHeight;
  final double cardWidth;
  final Widget creditItem;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: cardHeight,
      width: cardWidth,
      child: Card(
        shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: cardColor,
        child: creditItem,
      ),
    );
  }
}
