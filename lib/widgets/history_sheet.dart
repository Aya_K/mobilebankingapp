import 'package:flutter/material.dart';
import 'package:mobilebanking/util/HistoryItemsList.dart';
import 'package:mobilebanking/widgets/history_items.dart';
import 'package:mobilebanking/widgets/text_title.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class HistorySheet extends StatelessWidget {
  const HistorySheet(this.screenHeight, this.screenWidth , this.name , this.sideContent,{this.scrollable = false});
  final double screenHeight;
  final double screenWidth;
  final String name;
  final Widget sideContent;
  final bool scrollable;

  @override
  Widget build(BuildContext context) {
    HistoryList historyList = HistoryList();

    return SlidingUpPanel(
      maxHeight: scrollable == true ? screenHeight * .78 : screenHeight * .76,
      minHeight: scrollable == true ? screenHeight * .2 : screenHeight * .76,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0)),
      controller: PanelController(),
      header: SizedBox(
        width: screenWidth,
        height: screenHeight*0.1,
        child: Column(
          children: [
            SizedBox(
              height: 5,
            ),
            Container(
              height: 5,
              width: screenWidth * .1,
              color: Color(0xffE8E8EB),
            ),
            SizedBox(height: 10,),
            Container(
              color: Colors.white,
              width: screenWidth*.99,
              height: screenHeight*.07,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    scrollable == true ? TxtTitle(name) : Text(name , style: TextStyle(fontSize: 25 , color: Colors.black),),
                   sideContent,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      panelBuilder: (scrollController) => Padding(
        padding: const EdgeInsets.only(top: 40.0),
        child: ListView.builder(
          controller: scrollController,
          shrinkWrap: true,
          itemCount: historyList.historyItems.length,
          itemBuilder: (context, index) =>
              HistoryItems(historyList.historyItems[index]),
        ),
      ),
    );
  }
}
