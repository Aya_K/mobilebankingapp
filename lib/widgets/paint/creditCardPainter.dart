import 'package:flutter/material.dart';
class CreditCardPainter extends StatelessWidget {
  const CreditCardPainter(this.cardHeight,this.cardWidth);
  final double cardHeight;
  final double cardWidth;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: cardHeight,
      width: cardWidth,
      child: Stack(
        children: [
//          Image.asset('assets/card1.png',height: cardHeight, width: cardWidth,),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("CREDIT CARD" , style: TextStyle(color: Colors.white , fontSize: 15),),
                Spacer(),
                Text("\$ 1163", style: TextStyle(color: Colors.white , fontSize: 27),),
              ],
            ),
          ),
        ],

      ),
    );
  }
}
