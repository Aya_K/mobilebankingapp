import 'package:flutter/material.dart';

class OvalPainter extends CustomPainter {
  const OvalPainter(this.color, this.rectSize,this.dx, this.dy , {this.paintStyle = PaintingStyle.fill});
  final Color color;
  final PaintingStyle paintStyle;
  final Size rectSize;
  final double dx;
  final double dy;

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = color
      ..style = paintStyle;
    canvas.drawOval(Offset(dx, dy) & rectSize, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
