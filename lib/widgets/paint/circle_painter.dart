import 'package:flutter/material.dart';

class CirclePainter extends CustomPainter {
  const CirclePainter(this.color,  this.radius, this.dx, this.dy ,{this.paintStyle = PaintingStyle.fill});
  final Color color;
  final PaintingStyle paintStyle;
  final double radius;
  final double dx;
  final double dy;

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = color
      ..style = paintStyle;
    canvas.drawCircle(Offset(dx, dy), radius, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
