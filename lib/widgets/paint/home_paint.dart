import 'package:flutter/material.dart';
import 'package:mobilebanking/widgets/credit_card.dart';
import 'package:mobilebanking/widgets/paint/creditCardPainter.dart';
import 'package:mobilebanking/widgets/paint/oval_paint.dart';

class HomePaint extends StatelessWidget {
  const HomePaint(this.screenHeight,this.screenWidth);
  final double screenHeight;
  final double screenWidth;
  @override
  Widget build(BuildContext context) {

    return Container(
      height: screenHeight * 0.45,
      child: Stack(
        children: [
          CustomPaint(
            painter: OvalPainter(
                Color(0xff24346C),
                Size(screenWidth * 1.72, screenHeight * 0.56),
                screenWidth * -0.32,
                screenHeight * -0.15),
          ),
          Positioned(
            top: screenHeight * 0.05,
            left: screenWidth * .05,
            child: Text(
              "Wallet",
              style: TextStyle(
                color: Colors.white,
                fontSize: 30,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Positioned(
            top: screenHeight * 0.13,
            left: screenWidth * .14,
            child: CreditCard(screenHeight*.26,screenWidth*.66,Colors.grey),
          ),
          Positioned(
            top: screenHeight * 0.15,
            left: screenWidth * .12,
            child: CreditCard(screenHeight*.26,screenWidth*.71,Color(0xff352E2E)),
          ),
          Positioned(
            top: screenHeight * 0.17,
            left: screenWidth * .11,
            child: CreditCard(screenHeight*.26, screenWidth*.74,Color(0xffF5C8A0)),
          ),
          Positioned(
            top: screenHeight * 0.19,
            left: screenWidth * .1,
            child: CreditCard(screenHeight*.26,screenWidth*.77,Color(0xff24346C),creditItem: CreditCardPainter(screenHeight*.26,screenWidth*.77),),
          ),
        ],
        overflow: Overflow.visible,
      ),
    );
  }
}
