import 'package:flutter/material.dart';
import 'package:mobilebanking/widgets/paint/circle_painter.dart';
import 'package:mobilebanking/widgets/paint/oval_paint.dart';
import 'package:mobilebanking/widgets/text_title.dart';

class SignUpPaint extends StatelessWidget {
  const SignUpPaint({@required this.onPressed});
  final Function onPressed;
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return  Container(
      height: screenHeight * 0.21,
      child: Stack(
        children: [
          CustomPaint(
            painter: OvalPainter(
                Color(0xff24346C),
                Size(screenWidth * 0.7, screenHeight * 0.36),
                screenWidth * -0.06,
                screenHeight * -0.1),
          ),
          CustomPaint(
            size: Size(screenWidth * 0.5, screenHeight * 0.25),
            painter: CirclePainter(Color(0xffF56962), screenWidth * 0.37,
                screenWidth * 0.84, screenHeight * -0.1),
          ),
          CustomPaint(
            size: Size(screenWidth * 0.5, screenHeight * 0.25),
            painter: CirclePainter(Color(0xffF6CAC2), screenWidth * 0.24,
                screenWidth * 0.8, screenHeight * 0.15),
          ),
          Positioned(
            top: screenHeight * 0.08,
            left: screenWidth * .07,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TxtTitle("Create Account", txtColor: Colors.white,),
                Row(
                  children: [
                    Text("Already have an account?",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w600)),
                    InkWell(
                        onTap: (){print("Hi");},
                        child: Text("Sign In!",
                            style: TextStyle(
                                color: Color(0xffF56962),
                                fontSize: 18,
                                fontWeight: FontWeight.w600))),
                  ],
                ),
              ],
            ),
          ),
        ],
        overflow: Overflow.visible,
      ),
    );
  }
}
