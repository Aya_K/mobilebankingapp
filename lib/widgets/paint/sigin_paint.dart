import 'package:flutter/material.dart';
import 'package:mobilebanking/widgets/paint/circle_painter.dart';
import 'package:mobilebanking/widgets/paint/oval_paint.dart';
import 'package:mobilebanking/widgets/text_title.dart';

class SignInPaint extends StatelessWidget {
  const SignInPaint(this.screenWidth, this.screenHeight , {@required this.onPressed});
  final double screenWidth;
  final double screenHeight;
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight * 0.21,
      child: Stack(
        children: [
          CustomPaint(
            painter: OvalPainter(
                Color(0xffF56962),
                Size(screenWidth * 0.8, screenHeight * 0.54),
                screenWidth * 0.31,
                screenHeight * -0.2),
          ),
          CustomPaint(
            size: Size(screenWidth * 0.5, screenHeight * 0.25),
            painter: CirclePainter(Color(0xffF6CAC2), screenWidth * 0.28,
                screenWidth * 0.16, screenHeight * 0.14),
          ),
          Positioned(
            top: screenHeight * 0.14,
            left: screenWidth * .07,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TxtTitle("Sign In",txtColor: Colors.white,),
                Row(
                  children: [
                    Text("Don't have an account?",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w600)),
                    FlatButton(
                        onPressed: onPressed,
                        child: Text("Sign Up!",
                            style: TextStyle(
                                color: Color(0xff24346C),
                                fontSize: 18,
                                fontWeight: FontWeight.w600))),
                  ],
                ),
              ],
            ),
          ),
        ],
        overflow: Overflow.visible,
      ),
    );
  }
}
