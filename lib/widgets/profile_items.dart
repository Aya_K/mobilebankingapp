import 'package:flutter/material.dart';

class ProfileItems extends StatelessWidget {
  const ProfileItems(this.profileIcon,this.backColor,this.iconColor,this.name , {@required this.onPressed});
  final IconData profileIcon;
  final Color backColor;
  final Color iconColor;
  final String name;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPressed,
        child: ListTile(
        leading: Container(child: Icon(profileIcon , size: 25,color: iconColor,),
          height: 45,
          width: 45,
          decoration: BoxDecoration(
            color: backColor,
            borderRadius: BorderRadius.circular(13),
          ),
        ),
        title: Text(name , style: TextStyle(fontWeight: FontWeight.w600 , color: Colors.black),),
        trailing: Icon(Icons.arrow_forward_ios , color: Colors.black,),
    ),
      );
  }
}
