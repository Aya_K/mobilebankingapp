import 'package:flutter/material.dart';
import 'package:mobilebanking/models/historyItem.dart';

class HistoryItems extends StatelessWidget {
  const HistoryItems(this.history);
  final HistoryItem history;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(child: Icon(history.historyIcon , size: 23,color: Colors.white,),
        height: 45,
        width: 45,
        decoration: BoxDecoration(
          color: history.color,
          borderRadius: BorderRadius.circular(13),
        ),
      ),
      title: Text(history.name,style: TextStyle(color: Colors.black  , fontSize: 18 , fontWeight: FontWeight.w600),),
      subtitle: Text(history.dateTime ,style: TextStyle(fontSize: 18), ),
      trailing: Text("-\$"+history.bill.toString() , style: TextStyle(fontSize: 18 , fontWeight: FontWeight.w600),),
    );
  }
}
