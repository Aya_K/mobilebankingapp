import 'package:flutter/material.dart';

class OperationButton extends StatelessWidget {
  const OperationButton(this.cardIcon , this.iconColor ,{this.cardColor = Colors.white , this.label=""});
  final IconData cardIcon;
  final Color iconColor;
  final Color cardColor;
  final String label;
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Column(
      children: [
        SizedBox(
          height: screenWidth*0.18,
          width: screenWidth*0.18,
          child: RaisedButton(
            color: cardColor,
              onPressed: (){},
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Icon( cardIcon , color: iconColor, size: 30.0)),
        ),
        SizedBox(height: 5,),
        Text(label ,style: TextStyle(fontSize: 15 , fontWeight: FontWeight.w600),)
      ],
    );
  }
}
