import 'package:flutter/material.dart';

class HistoryItem{
  HistoryItem(this.historyIcon , this.color,this.name , this.dateTime , this.bill);
  IconData historyIcon;
  Color color;
  String name;
  String dateTime;
  double bill;
}