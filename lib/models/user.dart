class User {
  String username;
  String email;
  String phone;
  String password;

  User({String username, String email, String phone, String password}) {
    this.username = username;
    this.email = email;
    this.phone = phone;
    this.password = password;
  }

  Map<String, String> toMap() {
    return {
      "username": username,
      "email": email,
      "phone" : phone,
      "password" : password,
    };
  }
}
