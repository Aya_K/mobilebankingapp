import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobilebanking/screens/analytic_screen.dart';
import 'package:mobilebanking/screens/history_screen.dart';
import 'package:mobilebanking/screens/home_screen.dart';
import 'package:mobilebanking/screens/profile_screen.dart';

class Screen extends StatefulWidget {
  const Screen();

  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<Screen> {
  int _currentIndex = 0;
  final List<Widget> _screens = [
    HomeScreen(),
    HistoryScreen(),
    AnalyticScreen(),
    ProfileScreen(),
  ];

  void onTabTapped(int index) {
 setState(() {
            _currentIndex = index;
          });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _screens[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        unselectedItemColor: Colors.black26,
        selectedItemColor: Color(0xff24346C),
        onTap: onTabTapped,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        iconSize: 30,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.account_balance_wallet), title: Text("")),
          BottomNavigationBarItem(
              icon: FaIcon(FontAwesomeIcons.folderMinus), title: Text("")),
          BottomNavigationBarItem(
              icon: Icon(Icons.donut_small), title: Text("")),
          BottomNavigationBarItem(icon: Icon(Icons.person), title: Text("")),
        ],
      ),
//      BottomAppBar(
//        child: Padding(
//          padding: const EdgeInsets.all(8.0),
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//            children: [
//              IconButton(icon: Icon(Icons.account_balance_wallet), iconSize: 30, color: Color(0xffC5C6CE),focusColor: Color(0xff352E2E),onPressed: (){}),
//              IconButton(icon: Icon(Icons.folder), iconSize: 30, color: Color(0xffC5C6CE),onPressed: (){}),
//              IconButton(icon: Icon(Icons.donut_small), iconSize: 30, color: Color(0xffC5C6CE),onPressed: (){}),
//              IconButton(icon: Icon(Icons.person), iconSize: 30, color: Color(0xffC5C6CE),onPressed: (){}),
//            ],
//          ),
//        ),
    );
  }
}
