import 'package:flutter/material.dart';
import 'package:mobilebanking/screen.dart';
import 'package:mobilebanking/screens/splash_screen.dart';
import 'package:mobilebanking/util/util.dart';

class AppBase extends StatelessWidget {
  const AppBase();
  @override
  Widget build(BuildContext context) {
    print(Util.userStorage.read("user"));
    return MaterialApp(
      theme: ThemeData(primaryColor: Color(0xff24346C)),
      debugShowCheckedModeBanner: false,
      home: Util.userStorage.read("user") != null ? Screen() : SplashScreen(),
    );
  }
}
